package modelo;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.sql.DataSource;
import javax.swing.plaf.synth.SynthSeparatorUI;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.mindrot.jbcrypt.BCrypt;


@Named
@ApplicationScoped
public class ModeloDAO implements ModeloDAOInterfaz, Serializable {
	private static final Logger logger= Logger.getLogger(ModeloDAO.class.getName());
	private static final long serialVersionUID = 1L;
	public static LocalDate INICIO_REXISTRO_PEDIDOS = null;
	@Resource(lookup = "java:jboss/datasources/atenda_prime")
	private DataSource pool;

	public ModeloDAO() {
	}

	@PostConstruct
	public void init() {
		System.out.println("MODELODAO::: init() .....");
	}

	///// UsuarioDAO ///////////
	public Usuario autentica(String username, String password) throws Exception {
		Usuario usuario = null;
		Statement statement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		try {
			connection = pool.getConnection();
			statement = connection.createStatement();
			String sql = "SELECT * FROM usuario WHERE username = '" + username + "' ";
			resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				if (BCrypt.checkpw(password, resultSet.getString("password"))) {
					usuario = new Usuario();
					usuario.setId(resultSet.getInt("id"));
					usuario.setUsername(resultSet.getString("username"));
					usuario.setNome(resultSet.getString("nome"));
					usuario.setRol(resultSet.getString("rol"));
					usuario.setAvatar(resultSet.getString("avatar"));
				}
			}
		} finally {
			try { // captura excepci�ns no peche para que non oculte a que saltou no try principal
				resultSet.close();
				statement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
		return usuario;
	}

	public ArrayList<Usuario> getAllUsers() throws Exception {
		ArrayList<Usuario> usuarios = new ArrayList<Usuario>();
		Statement statement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		try {
			connection = pool.getConnection();
			statement = connection.createStatement();
			String sql = "SELECT * FROM usuario";
			resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				Usuario usuario = new Usuario();
				usuario.setId(resultSet.getInt("id"));
				usuario.setNome(resultSet.getString("nome"));
				usuario.setUsername(resultSet.getString("username"));
				usuario.setRol(resultSet.getString("rol"));
				usuario.setAvatar(resultSet.getString("avatar"));
				usuarios.add(usuario);
			}
		} finally {
			try { // captura excepci�ns no peche para que non oculte a que saltou no try principal
				resultSet.close();
				statement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
		return usuarios;
	}

	@Override
	public void actualiza(Usuario usuario) throws Exception {
		Statement statement = null;
		Connection connection = null;
		try {
			connection = pool.getConnection();
			statement = connection.createStatement();
			// se usuario trae pass ser� en claro para actualizar a do modelo
			String hashedPassword = BCrypt.hashpw(usuario.getPassword(), BCrypt.gensalt(12));
			String sql = "UPDATE usuario SET username='" + usuario.getUsername() + "',";
			if (usuario.getPassword() != null && !usuario.getPassword().isEmpty()) {
				sql += " password ='" + hashedPassword + "', ";
			}
			sql += " nome ='" + usuario.getNome() + "', rol='" + usuario.getRol() + "', avatar = '"
					+ usuario.getAvatar() + "' WHERE id ='" + usuario.getId() + "'";
			statement.executeUpdate(sql);
		} finally {
			try { // captura excepci�ns no peche para que non oculte a que saltou no try principal
				statement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
	}

	@Override
	public int inserta(Usuario usuario) throws Exception {
		int lastid = -1;
		Statement statement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		try {
			connection = pool.getConnection();
			statement = connection.createStatement();
			String hashedPassword = BCrypt.hashpw(usuario.getPassword(), BCrypt.gensalt(12));
			String sql = "INSERT INTO usuario  ( username, password, nome, rol, avatar)  VALUES ( '"
					+ usuario.getUsername() + "','" + hashedPassword + "','" + usuario.getNome() + "','"
					+ usuario.getRol() + "', '" + usuario.getAvatar() + "')";

			statement.executeUpdate(sql);
			String sql1 = "SELECT LAST_INSERT_ID() as lastid";
			resultSet = statement.executeQuery(sql1);
			while (resultSet.next()) {
				lastid = resultSet.getInt("lastid");
			}
		} finally {
			try { // captura excepci�ns no peche para que non oculte a que saltou no try principal
				resultSet.close();
				statement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
		return lastid;
	}

	@Override
	public void borra(Usuario usuario) throws Exception {
		Statement statement = null;
		Connection connection = null;
		try {
			connection = pool.getConnection();
			statement = connection.createStatement();
			String sql = "DELETE FROM usuario WHERE id='" + usuario.getId() + "'";
			statement.executeUpdate(sql);
		} finally {
			try { // captura excepci�ns no peche para que non oculte a que saltou no try principal
				statement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
	}

	@Override
	public boolean existe(Usuario usuario) throws Exception {
		Statement statement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		boolean atopado = false;
		try {
			connection = pool.getConnection();
			statement = connection.createStatement();
			String sql = "SELECT * FROM usuario WHERE id ='" + usuario.getId() + "' ";
			resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				atopado = true;
			}
		} finally {
			try { // captura excepci�ns no peche para que non oculte a que saltou no try principal
				resultSet.close();
				statement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
		return atopado;
	}

	@Override
	public boolean existeUsername(String username) throws Exception {
		Statement statement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		boolean atopado = false;
		try {
			connection = pool.getConnection();
			statement = connection.createStatement();
			String sql = "SELECT username FROM usuario WHERE username ='" + username + "' ";
			resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				atopado = true;
			}
		} finally {
			try { // captura excepci�ns no peche para que non oculte a que saltou no try principal
				resultSet.close();
				statement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
		return atopado;

	}

	public Usuario getUsuarioPorId(int idUsuario) throws Exception {
		Usuario usuario = null;
		Statement statement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		try {
			connection = pool.getConnection();
			statement = connection.createStatement();
			String sql = "SELECT * FROM usuario WHERE id ='" + idUsuario + "'";
			resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				usuario = new Usuario();
				usuario.setId(resultSet.getInt("id"));
				usuario.setNome(resultSet.getString("nome"));
				usuario.setUsername(resultSet.getString("username"));
				usuario.setRol(resultSet.getString("rol"));
				usuario.setAvatar(resultSet.getString("avatar"));
			}
		} finally {
			try { // captura excepci�ns no peche para que non oculte a que saltou no try principal
				resultSet.close();
				statement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
		return usuario;

	}

	///// ProdutoDAO ///////////
	public ArrayList<Produto> getAll() throws Exception {
		ArrayList<Produto> produtos = new ArrayList<Produto>();
		Statement statement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		try {
			connection = pool.getConnection();
			statement = connection.createStatement();
			String sql = "SELECT * FROM produto WHERE produto.baixa=false ";
			resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				Produto produto = new Produto();
				produto.setId(resultSet.getInt("id"));
				produto.setNome(resultSet.getString("nome"));
				produto.setPrezo(resultSet.getDouble("prezo"));
				produto.setDesconto(resultSet.getInt("desconto"));
				produto.setCoste(resultSet.getDouble("coste"));
				produto.setIva(resultSet.getInt("iva"));
				produto.setStock(resultSet.getInt("stock"));
				produto.setFoto(resultSet.getString("foto"));
				produto.setBaixa(resultSet.getBoolean("baixa"));
				produtos.add(produto);
			}
		} finally {
			try { // captura excepci�ns no peche para que non oculte a que saltou no try principal
				resultSet.close();
				statement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
		return produtos;
	}

	@Override
	public void actualiza(Produto produto) throws Exception {
		Statement statement = null;
		Connection connection = null;
		try {
			connection = pool.getConnection();
			statement = connection.createStatement();
//			if (produto.getIva() == null) {
//				produto.setIva(TipoIVA.TIPO_XERAL);
//			}
			String sql = "UPDATE produto SET nome='" + produto.getNome() + "', prezo ='" + produto.getPrezo()
					+ "', desconto ='" + produto.getDesconto() + "', coste='" + produto.getCoste() + "', iva='"
					+ String.valueOf(produto.getIva()) + "', stock='" + produto.getStock() + "' , foto ='"
					+ produto.getFoto() + "' , baixa=" + produto.isBaixa() + " WHERE id ='" + produto.getId() + "'";
			// System.out.println(sql);
			statement.executeUpdate(sql);
		} finally {
			try { // captura excepci�ns no peche para que non oculte a que saltou no try principal
				statement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
	}

	@Override
	public int inserta(Produto produto) throws Exception {
		// inserta produto sen ID e devolve o id asignado
		Produto produtoVacio = insertaVacio();
		produto.setId(produtoVacio.getId());
		actualiza(produto);
		return produto.getId();
	}

	@Override
	public Produto insertaVacio() throws Exception {
		int lastid = 0;
		Statement statement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		try {
			connection = pool.getConnection();
			statement = connection.createStatement();
			String sql1 = "INSERT INTO produto (nome, prezo, stock)  VALUES ('', 0, 0)";
			statement.executeUpdate(sql1);
			String sql = "SELECT LAST_INSERT_ID() as lastid";
			resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				lastid = resultSet.getInt("lastid");
			}
		} finally {
			try { // captura excepci�ns no peche para que non oculte a que saltou no try principal
				resultSet.close();
				statement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
		Produto p = new Produto();
		p.setId(lastid);
		p.setNome("nome...");
		p.setPrezo(0);
		p.setStock(0);
		return p;
	}

	@Override
	// refactorizar para non borrar produto, senon marcar como borrado
	public void borra(Produto produto) throws Exception {
		Statement statement = null;
		Connection connection = null;
		try {
			connection = pool.getConnection();
			statement = connection.createStatement();
			String sql = "UPDATE produto SET produto.baixa=1 WHERE id='" + produto.getId() + "'";
			statement.executeUpdate(sql);
		} finally {
			try { // captura excepci�ns no peche para que non oculte a que saltou no try principal
				statement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
	}

	public Produto getProdutoPorId(int idProduto) throws Exception {
		Produto produto = null;
		Statement statement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		try {
			connection = pool.getConnection();
			statement = connection.createStatement();
			String sql = "SELECT * FROM produto WHERE id ='" + idProduto + "' ";
			resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				produto = new Produto();
				produto.setId(resultSet.getInt("id"));
				produto.setNome(resultSet.getString("nome"));
				produto.setPrezo(resultSet.getDouble("prezo"));
				produto.setDesconto(resultSet.getInt("desconto"));
				produto.setCoste(resultSet.getDouble("coste"));
				produto.setIva(Integer.valueOf(resultSet.getString("iva")));
				produto.setStock(resultSet.getInt("stock"));
				produto.setFoto(resultSet.getString("foto"));
				produto.setBaixa(resultSet.getBoolean("baixa"));
			}
		} finally {
			try { // captura excepci�ns no peche para que non oculte a que saltou no try principal
				resultSet.close();
				statement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
		return produto;
	}

	/////////////// XML ///////////////////////
	@Override
	public void exportaProdutos(ArrayList<Produto> produtos, File exportedFile) {
	}

	@Override
	public void importaProdutos(File xmlFileProdutos) {
	}

	//////////// Pedido DAO////////////////////
	@Override
	public Pedido getPedidoPorId(int id) throws Exception {
		Pedido pedido = new Pedido();
		Statement statement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		String sql = "SELECT pedido.id as idPedido, pedido.id_pedido_orix as idPedidoOrix, pedido.data_hora as dataHora, pedido.pechado as pechado, pedido.recibido as recibido, linea_pedido.id as idLineaPedido, linea_pedido.desconto as descontoLineaPedido, "
				+ " linea_pedido.unidades as unidades, linea_pedido.prezo as prezo, linea_pedido.coste as coste, usuario.id as idUsuario, usuario.username as username, usuario.nome as nomeUsuario, usuario.rol as rol, usuario.avatar as avatar, produto.id as idProduto, produto.nome as nomeProduto, "
				+ " produto.prezo as prezoProduto, produto.desconto as descontoProduto, produto.coste as costeProduto, produto.iva as iva, produto.stock as stock, produto.foto as foto, produto.baixa as baixa FROM pedido, linea_pedido, usuario, produto "
				+ " WHERE pedido.id=" + id
				+ " AND linea_pedido.id_pedido=pedido.id AND produto.id= linea_pedido.id_produto "
				+ "AND usuario.id = pedido.id_cliente ";
		try {
			connection = pool.getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sql);
			ArrayList<LineaPedido> lineasPedido = new ArrayList<LineaPedido>();
			LineaPedido lineaPedido = null;
			Produto produto = null;
			Usuario usuario = null;
			while (resultSet.next()) {
				pedido.setId(resultSet.getInt("idPedido"));
				pedido.setIdPedidoOrix(resultSet.getInt("idPedidoOrix"));
				pedido.setData(resultSet.getTimestamp("dataHora").toLocalDateTime());
				pedido.setPechado(resultSet.getBoolean("pechado"));
				pedido.setRecibido(resultSet.getBoolean("recibido"));
				usuario = new Usuario();
				usuario.setId(resultSet.getInt("idUsuario"));
				usuario.setNome(resultSet.getString("nomeUsuario"));
				usuario.setUsername(resultSet.getString("username"));
				usuario.setRol(resultSet.getString("rol"));
				usuario.setAvatar(resultSet.getString("avatar"));
				pedido.setCliente(usuario);
				lineaPedido = new LineaPedido();
				lineaPedido.setId(resultSet.getInt("idLineaPedido"));
				lineaPedido.setDesconto(resultSet.getInt("descontoLineaPedido"));
				produto = new Produto();
				produto.setId(resultSet.getInt("idProduto"));
				produto.setNome(resultSet.getString("nomeProduto"));
				produto.setPrezo(resultSet.getDouble("prezoProduto"));
				produto.setDesconto(resultSet.getInt("descontoProduto"));
				produto.setCoste(resultSet.getDouble("costeProduto"));
				produto.setIva(Integer.parseInt(resultSet.getString("iva")));
				produto.setStock(resultSet.getInt("stock"));
				produto.setFoto(resultSet.getString("foto"));
				produto.setBaixa(resultSet.getBoolean("baixa"));
				lineaPedido.setProduto(produto);
				lineaPedido.setUnidades(resultSet.getInt("unidades"));
				lineaPedido.setPrezo(resultSet.getDouble("prezo"));
				lineaPedido.setCoste(resultSet.getDouble("coste"));
				lineasPedido.add(lineaPedido);
			}
			pedido.setLineasPedido(lineasPedido);
		} finally {
			try { // captura excepci�ns no peche para que non oculte a que saltou no try principal
				resultSet.close();
				statement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
		return pedido;
	}

	@Override
	public ArrayList<Pedido> getPedidosPeriodo(LocalDate dendeLocalDate, LocalDate ataLocalDate, boolean eDevolucions,
			boolean pechados, boolean recibido) throws Exception { // so devolve os pedidos pechados
		if (dendeLocalDate.isBefore(ModeloDAOInterfaz.INICIO_REXISTRO_PEDIDOS)) {
			throw new Exception("So se procesan pedidos dende "
					+ ModeloDAOInterfaz.INICIO_REXISTRO_PEDIDOS.format(DateTimeFormatter.ofPattern("d/L/y")));
		}

		Date dende = Date.valueOf(dendeLocalDate);
		ataLocalDate = ataLocalDate.plusDays(1);// PARA QUE COLLA TODOS OS PEDIDOS DO D�A DE HOXE.
		Date ata = Date.valueOf(ataLocalDate);

		String sql = "SELECT pedido.id as idPedido, pedido.id_pedido_orix as idPedidoOrix, pedido.data_hora as dataHora, pedido.pechado as pechado, pedido.recibido as recibido, linea_pedido.id as idLineaPedido, linea_pedido.desconto as descontoLineaPedido, "
				+ "linea_pedido.unidades as unidades,  linea_pedido.prezo as prezo, linea_pedido.coste as coste, usuario.id as idUsuario, usuario.username as username, usuario.nome as nomeUsuario, usuario.rol as rol, usuario.avatar as avatar, produto.id as idProduto, produto.nome as nomeProduto,  "
				+ "produto.prezo as prezoProduto, produto.desconto as descontoProduto, produto.coste as costeProduto, produto.iva as iva, produto.stock as stock, produto.foto as foto FROM pedido, linea_pedido, usuario, produto "
				+ "WHERE pedido.id=linea_pedido.id_pedido AND pedido.id_cliente=usuario.id AND linea_pedido.id_produto=produto.id "
				+ "AND pedido.data_hora>= '" + dende + "' AND pedido.data_hora <= '" + ata + "' AND pedido.pechado = "
				+ pechados + " AND pedido.recibido = " + recibido + " ";

		if (!eDevolucions) {
			sql += "AND pedido.id_pedido_orix = 0 ";
		}
		sql += " ORDER BY pedido.id DESC";
		System.out.println("ModeloDAO: getPedidosPeriodo: " + sql);
		ResultSet resultSet = null;
		Statement statement = null;
		Connection connection = null;
		ArrayList<Pedido> pedidos = new ArrayList<Pedido>();
		try {
			connection = pool.getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sql);
			Pedido pedido = null;
			Usuario usuario = null;
			ArrayList<LineaPedido> lineasPedido = null;
			int idPedido = -1;
			int idPedidoOrix = -1;
			while (resultSet.next()) {
				int idNextPedido = resultSet.getInt("idPedido");
				// System.out.println("empezo bucle: idPedido: "+idPedido+" idNextPedido:
				// "+idNextPedido);
				if (idPedido > -1 && idNextPedido == idPedido) { // xa hai pedido creado e non cambia
					// System.out.println("xa hai pedido: creo nova li�a");
					LineaPedido lineaPedido = new LineaPedido();
					lineaPedido.setId(resultSet.getInt("idLineaPedido"));
					lineaPedido.setDesconto(resultSet.getInt("descontoLineaPedido"));
					// System.out.println("creo novo produto");
					Produto produto = new Produto();
					produto.setId(resultSet.getInt("idProduto"));
					produto.setNome(resultSet.getString("nomeProduto"));
					produto.setPrezo(resultSet.getDouble("prezoProduto"));
					produto.setDesconto(resultSet.getInt("descontoProduto"));
					produto.setCoste(resultSet.getDouble("costeProduto"));
					produto.setIva(Integer.parseInt(resultSet.getString("iva")));
					produto.setStock(resultSet.getInt("stock"));
					produto.setFoto(resultSet.getString("foto"));
					lineaPedido.setProduto(produto);
					lineaPedido.setUnidades(resultSet.getInt("unidades"));
					lineaPedido.setPrezo(resultSet.getDouble("prezo"));
					lineaPedido.setCoste(resultSet.getDouble("coste"));
					// System.out.println("engado li�a �s li�as");
					lineasPedido.add(lineaPedido);
				} else if (idPedido == -1 || idNextPedido != idPedido) {// primeiro ou cambia
					if (idNextPedido != idPedido && idPedido != -1) {// Pecho o anterior: se cambia
						pedido.setLineasPedido(lineasPedido);
						pedidos.add(pedido);
					}
					// idPedidoOrix = resultSet.getInt("idPedidoOrix");
					// if (conDevolucions || (!conDevolucions && idPedidoDevol == 0)) {
					// todos os pedidos ou s� orixinais e � pedido orixinal
					// Abro un novo pedido: primeira vez ou cambia
					pedido = new Pedido();
					// System.out.println("creo novo pedido");
					idPedido = resultSet.getInt("idPedido");
					pedido.setId(idPedido);
					pedido.setIdPedidoOrix(resultSet.getInt("idPedidoOrix"));
					pedido.setData(resultSet.getTimestamp("dataHora").toLocalDateTime());
					pedido.setPechado(resultSet.getBoolean("pechado"));
					pedido.setRecibido(resultSet.getBoolean("recibido"));
					// System.out.println("creo novo usuario");
					usuario = new Usuario();
					usuario.setId(resultSet.getInt("idUsuario"));
					usuario.setNome(resultSet.getString("nomeUsuario"));
					usuario.setUsername(resultSet.getString("username"));
					usuario.setRol(resultSet.getString("rol"));
					usuario.setAvatar(resultSet.getString("avatar"));
					pedido.setCliente(usuario);
					// System.out.println("creo novo lineasPedido");
					lineasPedido = new ArrayList<LineaPedido>();
					LineaPedido lineaPedido = new LineaPedido();
					lineaPedido.setId(resultSet.getInt("idLineaPedido"));
					lineaPedido.setDesconto(resultSet.getInt("descontoLineaPedido"));
					// System.out.println("creo novo produto");
					Produto produto = new Produto();
					produto.setId(resultSet.getInt("idProduto"));
					produto.setNome(resultSet.getString("nomeProduto"));
					produto.setPrezo(resultSet.getDouble("prezoProduto"));
					produto.setDesconto(resultSet.getInt("descontoProduto"));
					produto.setCoste(resultSet.getDouble("costeProduto"));
					produto.setIva(Integer.parseInt(resultSet.getString("iva")));
					produto.setStock(resultSet.getInt("stock"));
					produto.setFoto(resultSet.getString("foto"));
					lineaPedido.setProduto(produto);
					lineaPedido.setUnidades(resultSet.getInt("unidades"));
					lineaPedido.setPrezo(resultSet.getDouble("prezo"));
					lineaPedido.setCoste(resultSet.getDouble("coste"));
					lineasPedido.add(lineaPedido);
					// }
				}
			}
			// pecho �ltimo pedido se existe
			if (idPedido > -1) {
				pedido.setLineasPedido(lineasPedido);
				pedidos.add(pedido);
			}
		} finally {
			try { // captura excepci�ns no peche para que non oculte a que saltou no try principal
				resultSet.close();
				statement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
		return pedidos;
	}

	public ArrayList<Pedido> getPedidosPeriodoDe(LocalDate dendeLocalDate, LocalDate ataLocalDate, boolean eDevolucions,
			Usuario usuario, boolean pechados, boolean recibido) throws Exception {
		ArrayList<Pedido> pedidosUsuario = new ArrayList<Pedido>();
		ArrayList<Pedido> todosOsPedidos = getPedidosPeriodo(dendeLocalDate, ataLocalDate, eDevolucions, pechados,
				recibido);
		for (Pedido pedido : todosOsPedidos) {
			if (pedido.getCliente().equals(usuario)) {
				pedidosUsuario.add(pedido);
			}
		}
		System.out.println("ModeloDAO: getPedidosPeriodoDe: " + pedidosUsuario);
		return pedidosUsuario;
	}

	@Override
	public ArrayList<Pedido> getDevolucionsDe(Pedido pedidoOrix) throws Exception {
		// so pedidos pechados
		ResultSet resultSet = null;
		Statement statement = null;
		Connection connection = null;
		ArrayList<Pedido> pedidos = new ArrayList<Pedido>();
		try {
			connection = pool.getConnection();
			statement = connection.createStatement();
			String sql = "SELECT pedido.id as idPedido, pedido.id_pedido_orix as idPedidoOrix, pedido.data_hora as dataHora, pedido.pechado as pechado, pedido.recibido as recibido, linea_pedido.id as idLineaPedido, linea_pedido.desconto as descontoLineaPedido, "
					+ "linea_pedido.unidades as unidades, linea_pedido.prezo as prezo, linea_pedido.coste as coste, usuario.id as idUsuario, usuario.username as username, usuario.nome as nomeUsuario, usuario.rol as rol, usuario.avatar as avatar, produto.id as idProduto, produto.nome as nomeProduto,  "
					+ "produto.prezo as prezo, produto.desconto as descontoProduto, produto.coste as costeProduto, produto.iva as iva, produto.stock as stock, produto.foto as foto, produto.baixa as baixa FROM pedido, linea_pedido, usuario, produto "
					+ "WHERE pedido.id=linea_pedido.id_pedido AND pedido.id_cliente=usuario.id AND linea_pedido.id_produto=produto.id AND pedido.id_pedido_orix= '"
					+ pedidoOrix.getId() + "' AND pedido.pechado=1 AND pedido.recibido=1 ORDER BY pedido.id ";

			// System.out.println(sql);
			resultSet = statement.executeQuery(sql);
			Pedido pedido = null;
			Usuario usuario = null;
			ArrayList<LineaPedido> lineasPedido = null;
			int idPedido = -1;
			while (resultSet.next()) {
				int idNextPedido = resultSet.getInt("idPedido");
				// System.out.println("empezo bucle: idPedido: "+idPedido+" idNextPedido:
				// "+idNextPedido);
				if (idPedido > -1 && idNextPedido == idPedido) { // xa hai pedido creado e non cambia
					// System.out.println("xa hai pedido: creo nova li�a");
					LineaPedido lineaPedido = new LineaPedido();
					lineaPedido.setId(resultSet.getInt("idLineaPedido"));
					lineaPedido.setDesconto(resultSet.getInt("descontoLineaPedido"));
					// System.out.println("creo novo produto");
					Produto produto = new Produto();
					produto.setId(resultSet.getInt("idProduto"));
					produto.setNome(resultSet.getString("nomeProduto"));
					produto.setPrezo(resultSet.getDouble("prezoProduto"));
					produto.setDesconto(resultSet.getInt("descontoProduto"));
					produto.setCoste(resultSet.getDouble("costeProduto"));
					produto.setIva(Integer.parseInt(resultSet.getString("iva")));
					produto.setStock(resultSet.getInt("stock"));
					produto.setFoto(resultSet.getString("foto"));
					produto.setBaixa(resultSet.getBoolean("baixa"));
					lineaPedido.setProduto(produto);
					lineaPedido.setUnidades(resultSet.getInt("unidades"));
					lineaPedido.setPrezo(resultSet.getDouble("prezo"));
					lineaPedido.setCoste(resultSet.getDouble("coste"));
					// System.out.println("engado li�a �s li�as");
					lineasPedido.add(lineaPedido);
				} else if (idPedido == -1 || idNextPedido != idPedido) {// primeiro ou cambia
					if (idNextPedido != idPedido && idPedido != -1) {// Pecho o anterior: se cambia
						pedido.setLineasPedido(lineasPedido);
						pedidos.add(pedido);
					}
					// todos os pedidos ou s� orixinais e � pedido orixinal
					// Abro un novo pedido: primeira vez ou cambia
					pedido = new Pedido();
					// System.out.println("creo novo pedido");
					idPedido = resultSet.getInt("idPedido");
					pedido.setId(idPedido);
					pedido.setIdPedidoOrix(resultSet.getInt("idPedidoOrix"));
					pedido.setData(resultSet.getTimestamp("dataHora").toLocalDateTime());
					pedido.setPechado(resultSet.getBoolean("pechado"));
					pedido.setRecibido(resultSet.getBoolean("recibido"));
					// System.out.println("creo novo usuario");
					usuario = new Usuario();
					usuario.setId(resultSet.getInt("idUsuario"));
					usuario.setNome(resultSet.getString("nomeUsuario"));
					usuario.setUsername(resultSet.getString("username"));
					usuario.setRol(resultSet.getString("rol"));
					usuario.setAvatar(resultSet.getString("avatar"));
					pedido.setCliente(usuario);
					// System.out.println("creo novo lineasPedido");
					lineasPedido = new ArrayList<LineaPedido>();
					LineaPedido lineaPedido = new LineaPedido();
					lineaPedido.setId(resultSet.getInt("idLineaPedido"));
					lineaPedido.setDesconto(resultSet.getInt("descontoLineaPedido"));
					// System.out.println("creo novo produto");
					Produto produto = new Produto();
					produto.setId(resultSet.getInt("idProduto"));
					produto.setNome(resultSet.getString("nomeProduto"));
					produto.setPrezo(resultSet.getDouble("prezoProduto"));
					produto.setDesconto(resultSet.getInt("descontoProduto"));
					produto.setCoste(resultSet.getDouble("costeProduto"));
					produto.setIva(Integer.parseInt(resultSet.getString("iva")));
					produto.setStock(resultSet.getInt("stock"));
					produto.setFoto(resultSet.getString("foto"));
					produto.setBaixa(resultSet.getBoolean("baixa"));
					lineaPedido.setProduto(produto);
					lineaPedido.setUnidades(resultSet.getInt("unidades"));
					lineaPedido.setPrezo(resultSet.getDouble("prezo"));
					lineaPedido.setCoste(resultSet.getDouble("coste"));
					lineasPedido.add(lineaPedido);
				}
			}
			// pecho �ltimo pedido se existe
			if (idPedido > -1) {
				pedido.setLineasPedido(lineasPedido);
				pedidos.add(pedido);
			}
		} finally {
			try { // captura excepci�ns no peche para que non oculte a que saltou no try principal
				resultSet.close();
				statement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
		return pedidos;
	}

	@Override
	public int inserta(Pedido pedido) throws Exception {
		// inserta pedido como NON PECHADO e NON RECIBIDO, insertando tam�n lineas
		// pedido que cont�n sen actualizar stocks
		// se non ten cliente asociado, deixalle null
		int lastid = -1;
		Statement statement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		try {
			connection = pool.getConnection();
			statement = connection.createStatement();

			String sql = "INSERT INTO pedido (id_cliente, id_pedido_orix, data_hora)  VALUES (";
			sql += (pedido.getCliente() == null) ? " 0 " : pedido.getCliente().getId(); // si ANON => cliente_id =0
			sql += (pedido.getIdPedidoOrix() == 0) ? ",  0 " : ", " + pedido.getIdPedidoOrix();
			sql += ", '" + java.sql.Timestamp.valueOf(pedido.getData()) + "')";
			System.out.println("ModeloDAO:: inserta pedido: " + sql);
			statement.executeUpdate(sql);
			String sql1 = "SELECT LAST_INSERT_ID() as lastid";
			resultSet = statement.executeQuery(sql1);
			while (resultSet.next()) {
				lastid = resultSet.getInt("lastid");
			}
		} finally {
			try { // captura excepcions no peche para que non oculte a que saltou no try principal
				resultSet.close();
				statement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
		pedido.setId(lastid);
		for (LineaPedido lineaPedido : pedido.getLineasPedido()) {
			lineaPedido.setId(inserta(lineaPedido, pedido));
			// actualiza(lineaPedido.getProduto()); // non actualiza stock de produto
		}
		return lastid;
	}

	@Override
	public void actualiza(Pedido pedido) throws Exception {
		Statement statement = null;
		Connection connection = null;
		try {
			connection = pool.getConnection();
			statement = connection.createStatement();
			String sql = "UPDATE pedido SET id_pedido_orix='" + pedido.getIdPedidoOrix() + "', id_cliente='"
					+ pedido.getCliente().getId() + "'," + " data_hora='" + pedido.getData() + "', pechado="
					+ pedido.isPechado() + ", recibido=" + pedido.isRecibido() + "  WHERE  id = '" + pedido.getId()
					+ "' ";
			statement.executeUpdate(sql);
		} finally {
			try { // captura excepci�ns no peche para que non oculte a que saltou no try principal
				statement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
		if (pedido.isPechado()) { // actualiza stocks de produto
			for (LineaPedido lineaPedido : pedido.getLineasPedido()) {
				// lineaPedido.setId(inserta(lineaPedido, pedido));
				actualiza(lineaPedido.getProduto()); // actualiza stocks de produto
			}
		}
	}

	/// novo
	public void borra(Pedido pedido) throws Exception {
		Statement statement = null;
		Connection connection = null;
		try {
			connection = pool.getConnection();
			statement = connection.createStatement();
			String sql = "DELETE FROM pedido WHERE id = " + pedido.getId();
			// por CASCADE en relacion 1:M borrará as lineasPedido
			System.out.println("ModeloDAO: borra pedido: " + pedido.getId());
			statement.executeUpdate(sql);
		} finally {
			try { // captura excepci�ns no peche para que non oculte a que saltou no try principal
				statement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
	}

	///// LineaPedidoDAO ////////
	public int inserta(LineaPedido lineaPedido, Pedido pedido) throws Exception {
		int lastid = -1;
		ResultSet resultSet = null;
		Statement statement = null;
		Connection connection = null;
		try {
			connection = pool.getConnection();
			statement = connection.createStatement();
			String sql = "INSERT INTO linea_pedido (id_pedido, id_produto, unidades, desconto, prezo, coste)  VALUES ("
					+ pedido.getId() + ", " + lineaPedido.getProduto().getId() + ", " + lineaPedido.getUnidades() + ", "
					+ lineaPedido.getDesconto() + ", " + lineaPedido.getPrezo() + ", " + lineaPedido.getCoste() + ")";
			statement.executeUpdate(sql);
			String sql1 = "SELECT LAST_INSERT_ID() as lastid";
			resultSet = statement.executeQuery(sql1);
			while (resultSet.next()) {
				lastid = resultSet.getInt("lastid");
			}
		} finally {
			try { // captura excepci�ns no peche para que non oculte a que saltou no try principal
				resultSet.close();
				statement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
		return lastid;
	}

	@Override
	public void actualiza(LineaPedido lineaPedido) throws Exception { // actualiza todo menos id e id_pedido
		Statement statement = null;
		Connection connection = null;
		try {
			connection = pool.getConnection();
			statement = connection.createStatement();
			String sql = "UPDATE linea_pedido SET id_produto ='" + lineaPedido.getProduto().getId() + "', desconto ='"
					+ lineaPedido.getDesconto() + "' " + ", unidades = '" + lineaPedido.getUnidades() + "', prezo ='"
					+ lineaPedido.getPrezo() + "', coste='" + lineaPedido.getCoste() + "' WHERE id = '"
					+ lineaPedido.getId() + "' ";
			statement.executeUpdate(sql);
		} finally {
			try { // captura excepci�ns no peche para que non oculte a que saltou no try principal
				statement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
	}

	@Override
	public int getUnidadesDevoltasDe(LineaPedido linea) throws Exception {
		// recuperar pedidos de devol do pedido de lineaPedido
		// para cada pedido de devol, buscar lineas pedido con mesmo id produto e
		// devolver array
		LineaPedido lineaPedido = getLineaPedidoPorId(linea.getId()); // en caso de que sólo manden id
		Statement statement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		try {
			connection = pool.getConnection();
			statement = connection.createStatement();
			// sumo as unidades devoltas en lineas pedido de pedidos devolución do de
			// "linea"
			// e co mesmo produto
			String sql = "SELECT SUM(unidades) as unidadesDevol FROM linea_pedido WHERE id_pedido IN (SELECT id FROM pedido WHERE pedido.id_pedido_orix="
					+ "(SELECT id_pedido FROM linea_pedido WHERE id = " + lineaPedido.getId() + ")) AND id_produto="
					+ lineaPedido.getProduto().getId();

			// System.out.println(sql);
			getLineaPedidoPorId(lineaPedido.getId());
			resultSet = statement.executeQuery(sql);
			if (resultSet.next()) {
				return resultSet.getInt("unidadesDevol");
			}
		} finally {
			try { // captura excepci�ns no peche para que non oculte a que saltou no try principal
				resultSet.close();
				statement.close();
				connection.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return 0;
	}

	private LineaPedido getLineaPedidoPorId(int id) throws Exception {
		Statement statement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		LineaPedido lineaPedido = new LineaPedido();
		try {
			connection = pool.getConnection();
			statement = connection.createStatement();
			String sql = "SELECT linea_pedido.id as idLineaPedido, linea_pedido.id_produto as idProduto, "
					+ "linea_pedido.desconto as descontoLineaPedido, linea_pedido.unidades as unidades, linea_pedido.prezo as prezo, "
					+ "linea_pedido.coste as coste, produto.nome as nomeProduto,"
					+ "produto.prezoProduto as prezo, produto.desconto as descontoProduto, produto.coste as costeProduto, produto.iva as iva, "
					+ "produto.stock as stock, produto.foto as foto, produto.baixa as baixa FROM linea_pedido, pedido, produto"
					+ " WHERE linea_pedido.id='" + id
					+ "' AND linea_pedido.id_pedido=pedido.id AND linea_pedido.id_produto=produto.id";

			// System.out.println(sql);
			resultSet = statement.executeQuery(sql);
			if (resultSet.next()) {
				lineaPedido = new LineaPedido();
				lineaPedido.setId(resultSet.getInt("idLineaPedido"));
				lineaPedido.setDesconto(resultSet.getInt("descontoLineaPedido"));
				Produto produto = new Produto();
				produto.setId(resultSet.getInt("idProduto"));
				produto.setNome(resultSet.getString("nomeProduto"));
				produto.setPrezo(resultSet.getDouble("prezoProduto"));
				produto.setDesconto(resultSet.getInt("descontoProduto"));
				produto.setCoste(resultSet.getDouble("costeProduto"));
				produto.setIva(Integer.parseInt(resultSet.getString("iva")));
				produto.setStock(resultSet.getInt("stock"));
				produto.setFoto(resultSet.getString("foto"));
				produto.setBaixa(resultSet.getBoolean("baixa"));
				lineaPedido.setProduto(produto);
				lineaPedido.setUnidades(resultSet.getInt("unidades"));
				lineaPedido.setPrezo(resultSet.getDouble("prezo"));
				lineaPedido.setCoste(resultSet.getDouble("coste"));
			}
		} finally {
			try { // captura excepci�ns no peche para que non oculte a que saltou no try principal
				resultSet.close();
				statement.close();
				connection.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lineaPedido;
	}

	/// novo
	public void borra(LineaPedido linea) throws Exception {
		Statement statement = null;
		Connection connection = null;
		try {
			connection = pool.getConnection();
			statement = connection.createStatement();
			String sql = "DELETE FROM linea_pedido WHERE id = " + linea.getId();
			System.out.println("ModeloDAO: borra lineapedido: " + linea.getId());
			statement.executeUpdate(sql);
		} finally {
			try { // captura excepci�ns no peche para que non oculte a que saltou no try principal
				statement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
	}

	///////////// OpinionDAO //////////////////
	@Override
	public ArrayList<Opinion> getOpinions(Produto produto) throws Exception {
		ArrayList<Opinion> opinions = new ArrayList<Opinion>();
		Statement statement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		try {
			connection = pool.getConnection();
			statement = connection.createStatement();
			String sql = "SELECT * FROM opinion WHERE idProduto = '" + produto.getId()
					+ "' ORDER BY opinion.data_hora DESC";
			resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				Opinion opinion = new Opinion();
				opinion.setId(resultSet.getInt("id"));
				Usuario usuario = this.getUsuarioPorId(resultSet.getInt("idUsuario"));
				opinion.setUsuario(usuario);
				opinion.setIdProduto(resultSet.getInt("idProduto"));
				opinion.setData(resultSet.getTimestamp("data_hora").toLocalDateTime());
				opinion.setValoracion(Integer.valueOf(resultSet.getString("valoracion")));
				opinion.setTexto(resultSet.getString("texto"));
				opinions.add(opinion);
			}
		} finally {
			try { // captura excepci�ns no peche para que non oculte a que saltou no try principal
				resultSet.close();
				statement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
		return opinions;
	}

	@Override
	public int inserta(Opinion opinion) throws Exception {
		int lastid = -1;
		Statement statement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		try {
			connection = pool.getConnection();
			statement = connection.createStatement();
			String sql = "INSERT INTO opinion  (idUsuario, idProduto, valoracion, texto, data_hora)  VALUES ( '"
					+ opinion.getUsuario().getId() + "','" + opinion.getIdProduto() + "','" + opinion.getValoracion()
					+ "','" + opinion.getTexto() + "', '" + java.sql.Timestamp.valueOf(opinion.getData()) + "' )";
			// System.out.println(sql);
			statement.executeUpdate(sql);
			String sql1 = "SELECT LAST_INSERT_ID() as lastid";
			resultSet = statement.executeQuery(sql1);
			while (resultSet.next()) {
				lastid = resultSet.getInt("lastid");
			}
		} finally {
			try { // captura excepci�ns no peche para que non oculte a que saltou no try principal
				resultSet.close();
				statement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
		return lastid;
	}

	public LinkedHashMap<Integer, Integer> getValoracions(Produto produto) throws Exception {
		LinkedHashMap<Integer, Integer> valoracions = new LinkedHashMap<Integer, Integer>();
		Statement statement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		try {
			connection = pool.getConnection();
			statement = connection.createStatement();
			String sql = "SELECT valoracion, COUNT(id) AS numOpinions FROM opinion WHERE idProduto='" + produto.getId()
					+ "' GROUP BY valoracion ORDER BY valoracion DESC";
			resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				// System.out.println("valoracion: "+resultSet.getInt("valoracion")+"
				// numOpinions: "+resultSet.getInt("numOpinions"));
				valoracions.put(resultSet.getInt("valoracion"), resultSet.getInt("numOpinions"));
			}
		} finally {
			try { // captura excepci�ns no peche para que non oculte a que saltou no try principal
				resultSet.close();
				statement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
		return valoracions;
	}

	@Override
	public int getValoracionMedia(Produto produto) throws Exception {
		LinkedHashMap<Integer, Integer> valoracions = getValoracions(produto);
		Integer totalValoracions = 0;
		Integer valoracionsPonderadas = 0;
		for (Map.Entry<Integer, Integer> entrada : valoracions.entrySet()) {
			Integer valoracion = entrada.getKey();
			Integer numValoracions = entrada.getValue();
			valoracionsPonderadas += valoracion * numValoracions;
			totalValoracions += numValoracions;
		}
		return (int) Math.round(valoracionsPonderadas.doubleValue() / totalValoracions.doubleValue());
	}

	/////////// BI ///////////////
	public Informe getInformePedidos(ArrayList<Pedido> pedidos) throws Exception {
		double facturacion = 0, facturacionPedidos = 0;
		double coste = 0, costePedidos = 0;
		double beneficio = 0, beneficioPedidos = 0;
		double desconto = 0, descontosPedidos = 0;
		double iva = 0, ivaPedidos = 0;

		for (Pedido pedido : pedidos) {
			for (LineaPedido lineaPedido : pedido.getLineasPedido()) {
				desconto = lineaPedido.getPrezo() * ((double) lineaPedido.getDesconto() / 100.0)
						* lineaPedido.getUnidades();
				facturacion = lineaPedido.getPrezo() * lineaPedido.getUnidades() - desconto;
				coste = lineaPedido.getCoste() * lineaPedido.getUnidades();
				beneficio = facturacion - coste;
				iva = facturacion * ((double) (lineaPedido.getProduto().getIva()) / 100.0);

				facturacionPedidos += facturacion;
				costePedidos += coste;
				beneficioPedidos += beneficio;
				descontosPedidos += desconto;
				ivaPedidos += iva;
			}
		}
		return new Informe().setFacturacion(round2Dec(facturacionPedidos)).setCoste(round2Dec(costePedidos))
				.setDescontos(round2Dec(descontosPedidos)).setBeneficio(round2Dec(beneficioPedidos))
				.setIva(round2Dec(ivaPedidos));
	}

	// redondea a dous decimais un double.
	private double round2Dec(double d) {
		return (Math.round(d * 100) / 100.0);
	}

}
